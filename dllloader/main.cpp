#include <windows.h>
#include <iostream>

/* Define a function pointer for our imported
 * function.
 * This reads as "introduce the new type func_ptr as the type: 
 *                pointer to a function returning an int and 
 *                taking no arguments.
 *
 * Make sure to use matching calling convention (__cdecl, __stdcall, ...)
 * with the exported function. __stdcall is the convention used by the WinAPI
 */
typedef int (__stdcall *func_ptr)();

int main()
{
  HINSTANCE dllHandle = LoadLibrary("dlltoload.dll");

  if (!dllHandle) {
	std::cout << "could not load the dynamic library" << std::endl;
	return EXIT_FAILURE;
  }

  // resolve function address here
  func_ptr func = (func_ptr)GetProcAddress(dllHandle, "func");
  if (!func) 
  {
    std::cout << "could not locate the function" << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "func() returned " << func() << std::endl;

  FreeLibrary(dllHandle);

  return EXIT_SUCCESS;
}